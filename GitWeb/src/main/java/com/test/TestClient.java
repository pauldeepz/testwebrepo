package com.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Set;

public class TestClient {

	public static void main(String[] args) {
		try {
			
			File file = new File(System.getProperty("user.dir")+"//test.properties");
			FileInputStream fileInput = new FileInputStream(file);
			Properties properties = new Properties();
			properties.load(fileInput);
			fileInput.close();

			Set<String> enuKeys = properties.stringPropertyNames();
			for(String propName:enuKeys) {
				
				if(propName.startsWith("SUPPORTED_")){
					String value = properties.getProperty(propName);
					String[] stringArr=value.split("\\|");
					System.out.println(propName + ": " + stringArr[0]+" "+stringArr[1]);
					
				}
				
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

		
	}
				
