package com.data;

public class Employee {
	
	
	private int id;
	private String name;
	private String email;
	private int dptId;
	private int salary;
	
	public Employee(){
		System.out.println("Created Employee");
	}
	public Employee(int id,String name,String email,int dptId,int sal){
		
		this.id=id;
		this.name=name;
		this.email=email;
		this.salary=sal;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getDptId() {
		return dptId;
	}
	public void setDptId(int dptId) {
		this.dptId = dptId;
	}
	public int getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		this.salary = salary;
	}

}
