package com.mapper;

import java.util.List;

import com.data.Employee;

public interface EmployeeMapper {
	
	public List<Employee> findAllEmployees();
	
    public Employee findEmployeeById(int empId);
    
    public int createEmployee(Employee e);
    
    public void updateEmployee(Employee e);
}
