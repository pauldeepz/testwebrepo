package com.service;

import java.util.List;

import com.data.Employee;
import com.mapper.EmployeeMapper;

public class EmployeeService {
	
	public static void main(String[] args) {
		
		EmployeeMapper mapper=MyBatisSqlSessionFactory.getSession().getMapper(EmployeeMapper.class);
		List<Employee> employees=mapper.findAllEmployees();
		for(Employee e :employees){
			
			System.out.println(e.getId()+" "+e.getDptId()+" "+e.getEmail()+" "+e.getName()+" "+e.getSalary());
		}
		
		System.out.println("FindById");
		Employee emp=mapper.findEmployeeById(100);
		System.out.println(emp.getName());
	}

}
