package com.test;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.Set;

import org.junit.Test;


public class JunitTest {

	@Test
	public void test() {
try {
			
			File file = new File(System.getProperty("user.dir")+"//test.properties");
			FileInputStream fileInput = new FileInputStream(file);
			Properties properties = new Properties();
			properties.load(fileInput);
			fileInput.close();
			File file2 = new File(System.getProperty("user.dir")+"//value.properties");
			FileInputStream fileInput2 = new FileInputStream(file2);
			Properties properties2 = new Properties();
			properties2.load(fileInput2);
			fileInput2.close();
			
			String msg=CommonUtil.validate(properties,properties2,"WOD_");
			assertTrue(msg,msg.isEmpty());
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testSupported() {
try {
			
			File file = new File(System.getProperty("user.dir")+"//test.properties");
			FileInputStream fileInput = new FileInputStream(file);
			Properties properties = new Properties();
			properties.load(fileInput);
			fileInput.close();
			File file2 = new File(System.getProperty("user.dir")+"//value.properties");
			FileInputStream fileInput2 = new FileInputStream(file2);
			Properties properties2 = new Properties();
			properties2.load(fileInput2);
			fileInput2.close();
			
			String msg=CommonUtil.validate(properties,properties2,"SUPPORTED_");
			assertTrue(msg,msg.isEmpty());
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
