package com.test;

import static org.junit.Assert.assertEquals;

import java.util.Properties;
import java.util.Set;

import org.apache.tomcat.util.codec.binary.StringUtils;

public class CommonUtil {
	
	public static String validate(Properties p1,Properties p2,String test){
		Set<String> enuKeys = p1.stringPropertyNames();
		
		String msg="";
		for(String propName:enuKeys) {
			
			if(propName.startsWith(test)){
				System.out.println("Property "+propName);
				String statusMessage=validatFact(p1.getProperty(propName),p2.getProperty(propName));
				if(!statusMessage.isEmpty()){
					msg=msg+" TEST FOR "+propName+" FAILED WITH : Change from Remote";
					msg=msg+validatFact(p1.getProperty(propName),p2.getProperty(propName));
					msg=msg+"\n";
				}
				
			}
			
		}
		return msg;
	}
	
	public static String validatFact(String test,String  value){
		String msg="";
		if(value!=null){
			String[] stringArr=value.split("\\|");
			if(!"Hello".equals(stringArr[0])){
				msg=msg+"Not a hello msg ";
				
			}
			if(!"Test".equals(stringArr[1])){
				
				msg=msg+"Not a test msg ";
			}
			
		}else{
			
			msg=msg+" Value is null";
		}
		
		return msg;
	}
	

}
